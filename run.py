import pmcu
import time
import threading


def main():
    # camera = pmcu.Camera("C:/Program Files (x86)/digiCamControl/CameraControlCmd.exe", "")
    camera = None

    # unit = pmcu.PMCU(camera=camera, port=None, step_mode="1/8", dim=(3, 3, 3), timeout=5, baudrate=9600, x_dir=-1)
    unit = pmcu.DummyPMCU(camera=None, dim=(3, 3, 3), timeout=5, baudrate=9600, x_dir=-1)

    # for i in range(2):
    #     unit.move(dmz=1, silent=False)
        # camera.take_image()

    # unit.set_step_mode(step_mode="1/32")

    cp = pmcu.ControlPanel(unit, camera=camera)
    cp.mainloop()

    unit.close()


if __name__ == "__main__":
    main()

