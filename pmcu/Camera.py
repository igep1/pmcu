from pmcu.constants import *
import subprocess


class Camera:
    def __init__(self, control_cmd_path, img_path, shutter_speed=None, iso=None):
        self.control_cmd_path = control_cmd_path
        self.img_path = img_path
        self.set_img_path(self.img_path)
        self.iso_list = AVAILABLE["iso"]
        self.shutter_speed_list = AVAILABLE["shutter_speed"]
        self.get_available_params()
        if iso:
            self.set_iso(iso)
        else:
            self.set_iso(self.iso_list[0])
        if shutter_speed:
            self.set_shutter_speed(shutter_speed)
        else:
            self.set_shutter_speed(self.shutter_speed_list[0])

    def run_command(self, command):
        full_command = self.control_cmd_path + " " + command
        p = subprocess.Popen(full_command, stdout=subprocess.PIPE, universal_newlines=True, shell=False)
        (output, err) = p.communicate()
        return output, err

    def get_available_params(self):
        pass

    def set_img_path(self, path):
        self.img_path = path
        self.run_command("/folder " + path)

    def set_iso(self, iso):
        self.run_command("/iso " + str(iso))
        self.iso = iso

    def set_shutter_speed(self, shutter_speed):
        self.run_command("/shutter 1/" + str(shutter_speed))
        self.shutter_speed = shutter_speed

    def take_image(self, autofocus=False):
        if autofocus:
            self.run_command("/capture")
        else:
            self.run_command("/capturenoaf")

    def set_preset(self, preset):
        if type(preset) == str:
            preset = PRESETS[preset]
        if preset["iso"] > 0:
            self.set_iso(preset["iso"])
        if preset["shutter_speed"] > 0:
            self.set_shutter_speed(preset["shutter_speed"])

