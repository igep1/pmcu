from .constants import *


def get_step_mode_name(step_mode):
    num = int(float(2**step_mode))
    if num == 1:
        return "1"
    else:
        return "1/" + str(num)


def norm_step(step, step_mode):
    step_norm = int(round(step * get_norm_factor(step_mode)))
    rounding_error = not (step * get_norm_factor(step_mode) == step_norm)
    return step_norm, rounding_error


def get_norm_factor(step_mode):
    return 2**(step_mode - (len(AVAILABLE["step_mode"]) - 1))



