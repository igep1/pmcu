import serial
from .exceptions import *
from .constants import *
from .utils import *
import serial.tools.list_ports as list_ports
import time
import threading
import math


class PMCU(serial.Serial):
    def __init__(self, port=None, camera=None, gui=None, preset=DEFAULT_PRESET, step_mode=5, dim=(10, 10, 10),
                 hs_attempts=2, init_wait=2, tx=TX, ty=TY, tz=TZ, x_dir=1, baudrate=9600, *args, **kwargs):
        self.gui = gui
        if not port:
            port = self.find_port()
        super(PMCU, self).__init__(port, baudrate, *args, **kwargs)
        if init_wait:
            time.sleep(init_wait)
        assert (type(hs_attempts) == int and hs_attempts > 0)
        self.hs_attempts = hs_attempts
        self.camera = camera
        self.mosaic_active = False
        self.asleep = False
        self.step_mode = 0
        self.step_size = [0, 0, 0]
        self.step_time = [tx, ty, tz]
        self.pos = [0, 0, 0]
        self.dim = dim
        assert (x_dir in (-1, 1))
        self.x_dir = x_dir
        self.init_x_dir = x_dir
        self.mpos = [0, 0, 0]
        if self.x_dir < 0:
            self.mpos[0] = self.dim[0] - 1
        self.set_preset(preset, silent=True)
        self.set_step_mode(step_mode, silent=True)
        self.available = True

    def scan_mosaic(self):
        if not self.asleep:
            while self.mpos[1] < self.dim[1]:
                while (self.x_dir > 0 and self.mpos[0] < self.dim[0]) or (self.x_dir < 0 <= self.mpos[0]):
                    while self.mpos[2] < self.dim[2]:
                        if self.mosaic_active:
                            self.move(dmz=1)
                            if self.gui:
                                self.gui.update_mpos()
                            self.mpos[2] += 1
                        else:
                            break
                    self.mpos[2] -= 1
                    if self.mosaic_active:
                        self.move(dmz=-self.dim[2])
                        if self.gui:
                            self.gui.update_mpos()
                        self.mpos[2] = 0
                    else:
                        break
                    if self.mosaic_active:
                        self.move(dmx=self.x_dir)
                        if self.gui:
                            self.gui.update_mpos()
                        self.mpos[0] += self.x_dir
                    else:
                        break
                self.mpos[0] -= self.x_dir
                self.x_dir *= -1
                if self.mosaic_active:
                    self.move(dmy=1)
                    if self.gui:
                        self.gui.update_mpos()
                    self.mpos[1] += 1
                else:
                    break
            self.mpos[2] -= 1
            self.x_dir = 1

            if self.gui and self.mosaic_active:
                self.gui.pause_mosaic(silent=True)
                self.gui.message("mosaic ended")
        else:
            raise SleepException

    def start_mosaic(self):
        if not self.asleep:
            self.mosaic_active = True
            threading.Thread(target=self.scan_mosaic).start()
        else:
            raise SleepException

    def pause_mosaic(self):
        self.mosaic_active = False

    def reset_mosaic(self):
        self.mpos[0] = 0
        if self.init_x_dir < 0:
            self.mpos[0] = self.dim[0] - 1
        self.mpos[0] = 0
        self.mpos[0] = 0

    def change_init_x_dir(self, x_dir):
        self.init_x_dir = x_dir
        self.x_dir = x_dir
        self.reset_mosaic()

    def move(self, dmx=0, dmy=0, dmz=0, preset=-1, xstep=0, ystep=0, zstep=0, norm=True, silent=False):
        if not self.asleep:
            # prev_preset = self.preset
            # if preset >= 0:
            #     self.set_preset(preset)
            if xstep == 0:
                xstep = self.step_size[0]
            if ystep == 0:
                ystep = self.step_size[1]
            if zstep == 0:
                zstep = self.step_size[2]
            rounding_error = [False, False, False]
            if norm:
                xstep, rounding_error[0] = norm_step(xstep, self.step_mode)
                ystep, rounding_error[1] = norm_step(ystep, self.step_mode)
                zstep, rounding_error[2] = norm_step(zstep, self.step_mode)

            if abs(dmx*xstep) > 0:
                self.write_hs(("x" + self.create_numstring(dmx*xstep)), silent=silent)
            if abs(dmy*ystep) > 0:
                self.write_hs(("y" + self.create_numstring(dmy*ystep)), silent=silent)
            if abs(dmz*zstep) > 0:
                self.write_hs(("z" + self.create_numstring(dmz*zstep)), silent=silent)

            # if preset >= 0:
            #     self.set_preset(prev_preset)
            return rounding_error
        else:
            raise SleepException

    def sleep(self, silent=False):
        self.asleep = not self.asleep
        self.write_hs("s", silent=silent)

    def tauten(self, direction, silent=False):
        if direction == DIR_LEFT:
            self.move(dmx=3, silent=silent)
            self.move(dmx=-3, silent=silent)
        elif direction == DIR_RIGHT:
            self.move(dmx=-3, silent=silent)
            self.move(dmx=3, silent=silent)
        elif direction == DIR_UP:
            self.move(dmx=3, silent=silent)
            self.move(dmx=-3, silent=silent)
        elif direction == DIR_DOWN:
            self.move(dmx=-3, silent=silent)
            self.move(dmx=3, silent=silent)

    def find_port(self):
        ports = list_ports.comports()
        devices = [ports[i].device for i in range(len(ports))]
        dummy = "d\n".encode()
        idx = -1
        for i, device in enumerate(devices):
            ser = serial.Serial(device, baudrate=9600, timeout=1)
            time.sleep(2)
            ser.write(dummy)
            line = ser.readline()
            if line == dummy:
                idx = i
                break
        if idx >= 0:
            self.report("connecting to " + devices[idx])
            return devices[idx]
        else:
            self.report("!device not found")
            raise DeviceNotFoundException

    def report(self, message):
        if self.gui:
            self.gui.report(str(message))
        else:
            print(str(message))

    def set_step_size(self, step_size, silent=False):
        self.step_size = step_size
        if not silent:
            self.report("step size set to" + str(step_size))

    def set_step_mode(self, step_mode, silent=False):
        if not self.asleep:
            if type(step_mode) == str:
                if step_mode == "1":
                    step_mode = 0
                else:
                    step_mode = int(math.log2(int(float(step_mode[2:]))))
            self.write_hs("m" + str(step_mode), silent=silent)
            self.step_mode = step_mode
        else:
            raise SleepException

    def set_preset(self, preset, silent=False):
        if type(preset) == str:
            preset = PRESETS[preset]
        self.set_step_mode(preset["step_mode"], silent=True)
        for i in range(3):
            self.step_size[i] = preset["step_size"][i]
        if self.camera:
            self.camera.set_preset(preset)
        if not silent:
            self.report("preset set to " + list(PRESETS.keys())[list(PRESETS.values()).index(preset)])

    def write_hs(self, command, hs_attempts=-1, silent=False):
        command = self.encode(command)
        if hs_attempts <= 0:
            hs_attempts = self.hs_attempts
        success = False
        for i in range(hs_attempts):
            self.write(command)
            time.sleep(self.estimate_time(command))
            if self.handshake(command, silent=silent):
                success = True
                break
        if not success and not silent:
            self.report("!handshake failed")
        return success

    def handshake(self, command, silent=False):
        try:
            line = ""
            for i in range(2):
                line = self.readline()
                if not line == "\r\n".encode():
                    if DEBUG_MODE or not silent:
                        print("hs " + str(i) + " " + str(line))
                    break
            return line == command
        except serial.SerialTimeoutException:
            self.report("!connection timed out")

    def estimate_time(self, command, norm=TIME_NORM):
        factor = 1.
        command = self.decode(command)
        res = 0
        if command[0] == "x":
            num = abs(self.parse_numstring(command[1:]))
            res = factor * self.step_time[0] * num / 1000.
        elif command[0] == "y":
            num = abs(self.parse_numstring(command[1:]))
            res = factor * self.step_time[1] * num / 1000.
        elif command[0] == "z":
            num = abs(self.parse_numstring(command[1:]))
            res = factor * self.step_time[2] * num / 1000.
        if norm:
            res /= get_norm_factor(self.step_mode)
        return res

    @staticmethod
    def encode(command):
        if type(command) == bytes:
            command = command.decode()
        if command[-1] != "\n":
            command += "\n"
        return command.encode()

    @staticmethod
    def decode(command):
        if type(command) == bytes:
            command = command.decode()
        if command[-1] == "\n":
            command = command[:-1]
        return command

    @staticmethod
    def create_numstring(num):
        assert (type(num) == int)
        assert (abs(num) < 1000)
        numstring = str(num)
        if num < 0:
            sign = "-"
            numstring = numstring[1:]
        else:
            sign = "+"
        return sign + "{:0>4}".format(numstring)

    @staticmethod
    def parse_numstring(numstring):
        assert (type(numstring) == str)
        sign = 1
        if numstring[0] == "-":
            sign = -1
        return sign * int(float(numstring[1:]))

    def check_folder(self, path, frmt):
        """check for folder update OR get camera feedback"""
        pass

    def find_monomer_plane(self):
        """norm z to monomer plane"""
        pass


class DummyPMCU(PMCU):
    def __init__(self, port=None, camera=None, gui=None, preset=0, step_mode=5, dim=(10, 10, 10), hs_attempts=2,
                 init_wait=2, tx=TX, ty=TY, tz=TZ, x_dir=1, baudrate=9600, *args, **kwargs):
        self.gui = gui
        if not port:
            port = self.find_port()
        if init_wait:
            time.sleep(init_wait)
        assert (type(hs_attempts) == int and hs_attempts > 0)
        self.hs_attempts = hs_attempts
        self.camera = camera
        self.mosaic_active = False
        self.asleep = False
        self.preset = preset
        self.step_mode = step_mode
        self.step_size = [0, 0, 0]
        self.step_time = [tx, ty, tz]
        self.pos = [0, 0, 0]
        self.dim = dim
        assert (x_dir in (-1, 1))
        self.x_dir = x_dir
        self.init_x_dir = x_dir
        self.mpos = [0, 0, 0]
        if self.x_dir < 0:
            self.mpos[0] = self.dim[0] - 1
        # self.set_preset(self.preset)
        self.set_step_mode(step_mode)
        self.available = True

    def find_port(self):
        return "Dummy"

    def write(self, data):
        pass

    def handshake(self, command, silent=False):
        if DEBUG_MODE and not silent:
            print("hs " + str(0) + " " + str(command))
        return True

    def close(self):
        pass


