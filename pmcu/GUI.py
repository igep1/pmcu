from tkinter import *
from tkinter import filedialog
from .constants import *
from .utils import *
import os
import datetime
import time


class ControlPanel(Tk):
    def __init__(self, pmcu, camera=None, size=10, font_name="Courier", preset=DEFAULT_PRESET, *args, **kwargs):
        super(ControlPanel, self).__init__(*args, **kwargs)
        self.states = []
        font = (font_name, size)
        self.option_add("*Font", font)
        pad = 0.5

        self.pmcu = pmcu
        self.camera = camera
        if not camera and self.pmcu.camera:
            self.camera = self.pmcu.camera

        self.pmcu.gui = self
        self.title("PMCU Control")

        self.frm_top = Frame(self)
        self.frm_bottom = Frame(self)
        self.frm_top.pack(side="top", fill="x")
        self.frm_bottom.pack(side="bottom")

        # =========================
        # Log Frame
        # =========================
        self.frm_log = Frame(self.frm_bottom)
        self.frm_log.grid(row=0, column=0, rowspan=3, sticky="n", padx=int(round(size * pad)),
                          pady=int(round(size * pad)))
        scrollbar = Scrollbar(self.frm_log)
        self.frm_log_btns = Frame(self.frm_log)
        self.frm_log_btns.pack(side=TOP)
        self.btn_clear_log = Button(self.frm_log_btns, text="clear log", command=self.clear_log,
                                    width=int(round(1 * size)))
        self.btn_clear_log.pack(side=RIGHT)
        scrollbar.pack(side=RIGHT, fill=Y)
        self.text_log = Text(self.frm_log, wrap=NONE, yscrollcommand=scrollbar.set,
                             height=int(round(size)), width=int(round(size * 3)))
        self.text_log.pack(side=LEFT)
        self.text_log.config(state=DISABLED)
        scrollbar.config(command=self.text_log.yview)

        # =========================
        # File Viewer Frame
        # =========================
        self.img_path = StringVar()
        self.img_path.set(os.getcwd().replace("\\", "/") + "/")
        self.frm_file_viewer = Frame(self.frm_bottom)
        self.frm_file_viewer.grid(row=0, column=2, rowspan=3, sticky="n",
                                  padx=int(round(size * pad)), pady=int(round(size * pad)))
        self.frm_file_viewer_header = Frame(self.frm_file_viewer)
        self.frm_file_viewer_header.pack(side="top")
        self.lbox_files = Listbox(self.frm_file_viewer,
                                  height=int(round(size)), width=int(round(size * 3.5)))
        self.lbox_files.bind("<Double-Button-1>", self.open_folder)
        scrollbar = Scrollbar(self.frm_file_viewer)

        scrollbar.config(command=self.lbox_files.yview)
        self.lbox_files.config(yscrollcommand=scrollbar.set)
        self.update_fileviewer()
        self.lbox_files.pack(side="left")
        scrollbar.pack(side="right", fill="y")
        self.entry_img_path = Entry(self.frm_file_viewer_header, textvariable=self.img_path,
                                    width=int(round(size * 2)))
        self.entry_img_path.bind("<FocusOut>", self.set_img_path)
        self.entry_img_path.bind("<Return>", self.set_img_path)
        self.entry_img_path.grid(row=0, column=1)
        self.btn_go_back_folder = Button(self.frm_file_viewer_header, text="back",
                                         command=self.go_back_folder)
        self.btn_go_back_folder.grid(row=0, column=0)
        self.btn_open_filechooser = Button(self.frm_file_viewer_header, text="choose",
                                           command=self.choose_img_path)
        self.btn_open_filechooser.grid(row=0, column=2)
        self.set_img_path()

        # =========================
        # Params Frame
        # =========================
        self.frm_params = Frame(self.frm_bottom)
        self.frm_params.grid(row=0, column=1, sticky="n", padx=int(round(size * pad)), pady=int(round(size * pad)))

        Label(self.frm_params, text="Preset").grid(row=0, column=0, sticky="w")
        self.preset = StringVar()
        self.preset.set(preset)
        self.opt_menu_preset = OptionMenu(self.frm_params, self.preset, command=self.set_preset, *list(PRESETS.keys()))
        self.opt_menu_preset.config(width=int(round(size)))
        self.opt_menu_preset.grid(row=0, column=1, columnspan=3)

        Label(self.frm_params, text="Step Mode").grid(row=1, column=0, sticky="w")
        self.step_mode = StringVar()
        self.step_mode.set(get_step_mode_name(self.pmcu.step_mode))
        self.opt_menu_step = OptionMenu(self.frm_params, self.step_mode, command=self.set_step_mode,
                                        *AVAILABLE["step_mode"])
        self.opt_menu_step.config(width=int(round(size)))
        self.opt_menu_step.grid(row=1, column=1, columnspan=3)

        Label(self.frm_params, text="x").grid(row=2, column=1)
        Label(self.frm_params, text="y").grid(row=2, column=2)
        Label(self.frm_params, text="z").grid(row=2, column=3)

        Label(self.frm_params, text="Round Err").grid(row=5, column=0, sticky="w")
        self.round_err = [False, False, False]
        self.label_x_round_err = Label(self.frm_params, text="")
        self.label_y_round_err = Label(self.frm_params, text="")
        self.label_z_round_err = Label(self.frm_params, text="")
        self.label_x_round_err.grid(row=5, column=1)
        self.label_y_round_err.grid(row=5, column=2)
        self.label_z_round_err.grid(row=5, column=3)
        self.update_round_error()

        Label(self.frm_params, text="Step Size").grid(row=3, column=0, sticky="w")
        self.xstep = IntVar()
        self.xstep.set(self.pmcu.step_size[0])
        self.ystep = IntVar()
        self.ystep.set(self.pmcu.step_size[1])
        self.zstep = IntVar()
        self.zstep.set(self.pmcu.step_size[2])
        self.spinbox_xstep = Spinbox(self.frm_params, textvariable=self.xstep, command=self.set_step_size,
                                     from_=0, to=999, width=int(round(0.3 * size)))
        self.spinbox_ystep = Spinbox(self.frm_params, textvariable=self.ystep, command=self.set_step_size,
                                     from_=0, to=999, width=int(round(0.3 * size)))
        self.spinbox_zstep = Spinbox(self.frm_params, textvariable=self.zstep, command=self.set_step_size,
                                     from_=0, to=999, width=int(round(0.3 * size)))
        self.spinbox_xstep.bind("<FocusOut>", self.set_step_size)
        self.spinbox_ystep.bind("<FocusOut>", self.set_step_size)
        self.spinbox_zstep.bind("<FocusOut>", self.set_step_size)
        self.spinbox_xstep.bind("<Return>", self.set_step_size)
        self.spinbox_ystep.bind("<Return>", self.set_step_size)
        self.spinbox_zstep.bind("<Return>", self.set_step_size)
        self.spinbox_xstep.grid(row=3, column=1)
        self.spinbox_ystep.grid(row=3, column=2)
        self.spinbox_zstep.grid(row=3, column=3)

        Label(self.frm_params, text="Norm").grid(row=4, column=0, sticky="w")
        self.xstep_norm = IntVar()
        self.ystep_norm = IntVar()
        self.zstep_norm = IntVar()
        self.update_step_size_norm()
        self.spinbox_xstep_norm = Spinbox(self.frm_params, textvariable=self.xstep_norm,
                                          command=self.set_step_size_norm, from_=0, to=999,
                                          width=int(round(0.3 * size)))
        self.spinbox_ystep_norm = Spinbox(self.frm_params, textvariable=self.ystep_norm,
                                          command=self.set_step_size_norm, from_=0, to=999,
                                          width=int(round(0.3 * size)))
        self.spinbox_zstep_norm = Spinbox(self.frm_params, textvariable=self.zstep_norm,
                                          command=self.set_step_size_norm, from_=0, to=999,
                                          width=int(round(0.3 * size)))
        self.spinbox_xstep_norm.bind("<FocusOut>", self.set_step_size_norm)
        self.spinbox_ystep_norm.bind("<FocusOut>", self.set_step_size_norm)
        self.spinbox_zstep_norm.bind("<FocusOut>", self.set_step_size_norm)
        self.spinbox_xstep_norm.bind("<Return>", self.set_step_size_norm)
        self.spinbox_ystep_norm.bind("<Return>", self.set_step_size_norm)
        self.spinbox_zstep_norm.bind("<Return>", self.set_step_size_norm)
        self.spinbox_xstep_norm.grid(row=4, column=1)
        self.spinbox_ystep_norm.grid(row=4, column=2)
        self.spinbox_zstep_norm.grid(row=4, column=3)
        self.norm_move = BooleanVar()
        self.norm_move.set(True)
        self.check_norm = Checkbutton(self.frm_params, variable=self.norm_move)
        self.check_norm.grid(row=4, column=4)

        # =========================
        # Mosaic Control Frame
        # =========================
        self.frm_mosaic = Frame(self.frm_bottom)
        self.frm_mosaic.grid(row=1, column=1, sticky="s", padx=int(round(size * pad)), pady=int(round(size * pad)))

        self.mx = StringVar()
        self.my = StringVar()
        self.mz = StringVar()
        self.label_mx = Label(self.frm_mosaic, textvariable=self.mx,
                              width=int(round(0.3 * size)), height=int(round(0.25 * size)))
        self.label_my = Label(self.frm_mosaic, textvariable=self.my,
                              width=int(round(0.3 * size)), height=int(round(0.25 * size)))
        self.label_mz = Label(self.frm_mosaic, textvariable=self.mz,
                              width=int(round(0.3 * size)), height=int(round(0.25 * size)))
        self.label_mx.grid(row=0, column=0)
        self.label_my.grid(row=0, column=1)
        self.label_mz.grid(row=0, column=2)
        self.update_mpos()

        self.dim_x = IntVar()
        self.dim_x.set(self.pmcu.dim[0])
        self.dim_y = IntVar()
        self.dim_y.set(self.pmcu.dim[1])
        self.dim_z = IntVar()
        self.dim_z.set(self.pmcu.dim[2])
        self.spinbox_dim_x = Spinbox(self.frm_mosaic, textvariable=self.dim_x, command=self.update_dim,
                                     from_=0, to=99,
                                     width=int(round(0.3 * size)))
        self.spinbox_dim_y = Spinbox(self.frm_mosaic, textvariable=self.dim_y, command=self.update_dim,
                                     from_=0, to=99,
                                     width=int(round(0.3 * size)))
        self.spinbox_dim_z = Spinbox(self.frm_mosaic, textvariable=self.dim_z, command=self.update_dim,
                                     from_=0, to=99,
                                     width=int(round(0.3 * size)))
        self.spinbox_dim_x.bind("<FocusOut>", self.set_dim)
        self.spinbox_dim_y.bind("<FocusOut>", self.set_dim)
        self.spinbox_dim_z.bind("<FocusOut>", self.set_dim)
        self.spinbox_dim_x.bind("<Return>", self.set_dim)
        self.spinbox_dim_y.bind("<Return>", self.set_dim)
        self.spinbox_dim_z.bind("<Return>", self.set_dim)
        self.spinbox_dim_x.grid(row=1, column=0)
        self.spinbox_dim_y.grid(row=1, column=1)
        self.spinbox_dim_z.grid(row=1, column=2)
        self.update_dim()

        self.btn_mosaic = Button(self.frm_mosaic, command=self.start_pause_btn_pressed,
                                 text="start", width=int(round(0.5 * size)), height=int(round(0.25 * size)))
        self.btn_mosaic.grid(row=0, column=3)
        self.btn_reset = Button(self.frm_mosaic, command=self.reset_mosaic,
                                text="reset", width=int(round(0.5 * size)), height=int(round(0.25 * size)))
        self.btn_reset.grid(row=1, column=3)
        self.btn_lock_reset = Button(self.frm_mosaic, command=self.lock_reset,
                                     text="lock", width=int(round(0.5 * size)),
                                     height=int(round(0.25 * size)))
        self.lock_reset()
        self.btn_lock_reset.grid(row=1, column=4)

        self.x_dir = self.pmcu.init_x_dir
        self.btn_x_dir = Button(self.frm_mosaic, command=self.change_x_dir,
                                text="left" if self.x_dir < 0 else "right",
                                width=int(round(0.5 * size)), height=int(round(0.25 * size)))
        self.btn_x_dir.grid(row=0, column=4)

        # =========================
        # Movement Control Frame
        # =========================
        self.frm_movement = Frame(self.frm_top)
        self.frm_movement.pack(side="left", fill="x", padx=int(round(size * pad)), pady=int(round(size * pad)))
        self.btn_sleep = Button(self.frm_movement, text="sleep", command=self.sleep,
                                width=int(round(1 * size)), height=int(round(0.25 * size)))
        self.btn_tauten = Button(self.frm_movement, text="tauten", command=self.tauten_pressed,
                                 width=int(round(1 * size)), height=int(round(0.25 * size)))
        self.btn_find_monomer_plane = Button(self.frm_movement, text="mono plane", command=self.find_monomer_plane,
                                             width=int(round(1 * size)), height=int(round(0.25 * size)))
        self.btn_sleep.grid(row=0, column=0)
        self.btn_tauten.grid(row=1, column=0)
        self.btn_find_monomer_plane.grid(row=2, column=0)

        self.btn_left = Button(self.frm_movement, text="left", padx=0, pady=0,
                               width=int(round(0.5 * size)), height=int(round(0.25 * size)),
                               command=lambda: self.tauten(DIR_LEFT) if self.btn_tauten["relief"] == SUNKEN else
                               self.move(DIR_LEFT))
        self.btn_right = Button(self.frm_movement, text="right", padx=0, pady=0,
                                width=int(round(0.5 * size)), height=int(round(0.25 * size)),
                                command=lambda: self.tauten(DIR_RIGHT) if self.btn_tauten["relief"] == SUNKEN else
                                self.move(DIR_RIGHT))
        self.btn_up = Button(self.frm_movement, text="up", padx=0, pady=0,
                             width=int(round(0.5 * size)), height=int(round(0.25 * size)),
                             command=lambda: self.tauten(DIR_UP) if self.btn_tauten["relief"] == SUNKEN else
                             self.move(DIR_UP))
        self.btn_down = Button(self.frm_movement, text="down", padx=0, pady=0,
                               width=int(round(0.5 * size)), height=int(round(0.25 * size)),
                               command=lambda: self.tauten(DIR_DOWN) if self.btn_tauten["relief"] == SUNKEN else
                               self.move(DIR_DOWN))
        self.btn_in = Button(self.frm_movement, text="in", padx=0, pady=0,
                             width=int(round(0.5 * size)), height=int(round(0.25 * size)),
                             command=lambda: self.move(DIR_IN))
        self.btn_out = Button(self.frm_movement, text="out", padx=0, pady=0,
                              width=int(round(0.5 * size)), height=int(round(0.25 * size)),
                              command=lambda: self.move(DIR_OUT))
        self.btn_left.grid(row=1, column=2)
        self.btn_right.grid(row=1, column=4)
        self.btn_up.grid(row=0, column=3)
        self.btn_down.grid(row=2, column=3)
        self.btn_in.grid(row=2, column=5)
        self.btn_out.grid(row=0, column=5)
        Frame(self.frm_movement, width=int(round(size * 3))).grid(row=0, column=1)

        # =========================
        # Camera Control Frame
        # =========================
        self.frm_camera = Frame(self.frm_top)
        self.frm_camera.pack(side="right", fill="x", padx=int(round(size * pad)), pady=int(round(size * pad)))
        self.btn_capture = Button(self.frm_camera, text="capture",
                                  width=int(round(size)), height=int(round(0.25 * size)),
                                  command=self.take_img)
        self.btn_capture.grid(row=1, column=0)
        if self.camera:
            self.iso_list = [str(x) for x in self.camera.iso_list if x > 0]
            self.shutter_speed_list = ["1/" + str(x) for x in self.camera.shutter_speed_list if x > 0]
        else:
            self.iso_list = [str(x) for x in AVAILABLE["iso"] if x > 0]
            self.shutter_speed_list = ["1/" + str(x) for x in AVAILABLE["shutter_speed"] if x > 0]
        self.file_format_list = ["jpg", "nef", "jpg + nef"]
        self.iso = StringVar()
        self.shutter_speed = StringVar()
        self.file_format = StringVar()
        self.file_format.set("jpg + nef")
        if self.camera:
            self.iso.set(str(self.camera.iso))
            self.shutter_speed.set("1/" + str(self.camera.shutter_speed))
        else:
            self.iso.set(self.iso_list[0])
            self.shutter_speed.set(self.shutter_speed_list[0])
        Frame(self.frm_camera, width=int(round(size * 3))).grid(row=0, column=1)
        Label(self.frm_camera, text="ISO").grid(row=0, column=2, sticky="w")
        Label(self.frm_camera, text="Shutter Speed").grid(row=1, column=2, sticky="w")
        Label(self.frm_camera, text="File Format").grid(row=2, column=2, sticky="w")
        self.opt_menu_iso = OptionMenu(self.frm_camera, self.iso, command=self.set_iso, *self.iso_list)
        self.opt_menu_iso.config(width=int(round(size)))
        self.opt_menu_shutter_speed = OptionMenu(self.frm_camera, self.shutter_speed, command=self.set_shutter_speed,
                                                 *self.shutter_speed_list)
        self.opt_menu_shutter_speed.config(width=int(round(size)))
        self.opt_menu_file_format = OptionMenu(self.frm_camera, self.file_format, command=self.set_file_format,
                                               *self.file_format_list)
        self.opt_menu_file_format.config(width=int(round(size)))
        self.opt_menu_iso.grid(row=0, column=3)
        self.opt_menu_shutter_speed.grid(row=1, column=3)
        self.opt_menu_file_format.grid(row=2, column=3)

        # =========================
        # Initialize Variables
        # =========================
        self.set_preset(silent=True)

    def get_states(self, parent, states):
        sub_states = []
        for child in parent.winfo_children():
            if type(child) == Frame:
                sub_states = self.get_states(child, sub_states)
            elif type(child) in (Button, Spinbox, OptionMenu, Entry, Listbox):
                sub_states.append(child["state"])
            else:
                sub_states.append(None)
        states.append(sub_states)
        return states

    def set_states(self, parent, states=None, ex=None):
        if not ex:
            ex = []
        for i, child in enumerate(parent.winfo_children()):
            if type(child) == Frame:
                if type(states) == list:
                    self.set_states(child, states[i], ex=ex)
                else:
                    self.set_states(child, states, ex=ex)
            elif type(child) in (Button, Spinbox, OptionMenu, Entry):
                if child not in ex:
                    if type(states) == list:
                        child.config(state=states[i])
                    else:
                        child.config(state=states)

    def toggle_lock_widgets(self, ex=None):
        if not self.states:
            self.states = self.get_states(self, [])[0]
            self.set_states(self, states=DISABLED, ex=ex)
        else:
            self.set_states(self, states=self.states, ex=ex)
            self.states = []

    def take_img(self):
        if self.camera:
            self.camera.take_image()

    def sleep(self, silent=False):
        if not self.pmcu.asleep:
            if not silent:
                self.report("going to sleep")
            self.btn_sleep.config(text="wake up")
            self.toggle_lock_widgets(ex=[self.btn_sleep, self.btn_clear_log])
        else:
            if not silent:
                self.report("waking up")
            self.btn_sleep.config(text="sleep")
            self.toggle_lock_widgets(ex=[self.btn_sleep])
        self.pmcu.sleep(silent=True)

    def tauten_pressed(self):
        if self.btn_tauten["relief"] == RAISED:
            self.btn_tauten.config(relief=SUNKEN)
            self.toggle_lock_widgets(ex=[self.btn_tauten, self.btn_left, self.btn_right, self.btn_up, self.btn_down])
        else:
            self.btn_tauten.config(relief=RAISED)
            self.toggle_lock_widgets(ex=[self.btn_tauten, self.btn_left, self.btn_right, self.btn_up, self.btn_down])

    def tauten(self, d, silent=False):
        if not silent:
            self.report("tautening " + DIR_NAMES[d])
        self.pmcu.tauten(d, silent=True)
        self.tauten_pressed()

    def find_monomer_plane(self):
        self.pmcu.find_monomer_plane()

    def move(self, direction, silent=False):
        self.toggle_lock_widgets(ex=[self.btn_clear_log])
        k = int(direction/2)
        q = 2 * (direction % 2) - 1
        if k == 0:
            self.pmcu.move(dmx=q, norm=self.norm_move.get(), silent=True)
            if not silent:
                if self.norm_move.get():
                    self.report("moving " + DIR_NAMES[direction] + " " + str(abs(q * self.xstep_norm.get())))
                else:
                    self.report("moving " + DIR_NAMES[direction] + " " + str(abs(q * self.xstep.get())))
        elif k == 1:
            self.pmcu.move(dmy=q, norm=self.norm_move.get(), silent=True)
            if not silent:
                if self.norm_move.get():
                    self.report("moving " + DIR_NAMES[direction] + " " + str(abs(q * self.ystep_norm.get())))
                else:
                    self.report("moving " + DIR_NAMES[direction] + " " + str(abs(q * self.ystep.get())))
        else:
            self.pmcu.move(dmz=q, norm=self.norm_move.get(), silent=True)
            if not silent:
                if self.norm_move.get():
                    self.report("moving " + DIR_NAMES[direction] + " " + str(abs(q * self.zstep_norm.get())))
                else:
                    self.report("moving " + DIR_NAMES[direction] + " " + str(abs(q * self.zstep.get())))

        self.toggle_lock_widgets(ex=[self.btn_clear_log])

    def start_pause_btn_pressed(self):
        self.pause_mosaic() if self.pmcu.mosaic_active else self.start_mosaic()

    def start_mosaic(self, silent=False):
        if not silent:
            self.report("mosaic started")
        self.btn_mosaic.config(text="stop")
        self.toggle_lock_widgets(ex=[self.btn_mosaic, self.btn_clear_log])
        self.pmcu.start_mosaic()

    def pause_mosaic(self, silent=False):
        if not silent:
            self.report("mosaic paused")
        self.btn_mosaic.config(text="start")
        self.pmcu.pause_mosaic()
        self.toggle_lock_widgets(ex=[self.btn_mosaic, self.btn_clear_log])

    def reset_mosaic(self, silent=False):
        if not silent:
            self.report("mosaic reset")
        if self.pmcu.mosaic_active:
            self.pmcu.pause_mosaic()
            self.btn_mosaic.config(text="start")
        self.pmcu.reset_mosaic()
        self.update_mpos()
        self.lock_reset()

    def lock_reset(self):
        if self.btn_reset["state"] == NORMAL:
            self.btn_lock_reset.config(text="unlock")
            self.btn_reset.config(state=DISABLED)
            self.spinbox_dim_x.config(state=DISABLED)
            self.spinbox_dim_y.config(state=DISABLED)
            self.spinbox_dim_z.config(state=DISABLED)
        else:
            self.btn_lock_reset.config(text="lock")
            self.btn_reset.config(state=NORMAL)
            self.spinbox_dim_x.config(state=NORMAL)
            self.spinbox_dim_y.config(state=NORMAL)
            self.spinbox_dim_z.config(state=NORMAL)

    def change_x_dir(self):
        if self.x_dir < 0:
            self.btn_x_dir.config(text="right")
        else:
            self.btn_x_dir.config(text="left")
        self.x_dir *= -1
        self.pmcu.change_init_x_dir(self.x_dir)
        self.update_mpos()

    def update_mpos(self):
        self.mx.set("x\n" + str(self.pmcu.mpos[0]+1))
        self.my.set("y\n" + str(self.pmcu.mpos[1]+1))
        self.mz.set("z\n" + str(self.pmcu.mpos[2]+1))

    def set_dim(self, event=None):
        self.dim_x.set(self.spinbox_dim_x.get())
        self.dim_y.set(self.spinbox_dim_y.get())
        self.dim_z.set(self.spinbox_dim_z.get())
        self.update_dim()

    def update_dim(self):
        self.pmcu.dim = (self.dim_x.get(), self.dim_y.get(), self.dim_z.get())
        self.pmcu.reset_mosaic()
        self.update_mpos()

    def update_fileviewer(self):
        if self.camera:
            self.camera.img_path = self.img_path.get()
        files = os.listdir(self.img_path.get())
        self.lbox_files.delete(0, END)
        for file in files:
            self.lbox_files.insert(END, file)

    def set_img_path(self, event=None):
        entry_string = self.entry_img_path.get()
        if not entry_string.endswith("/"):
            entry_string += "/"
        if os.path.exists(entry_string):
            self.img_path.set(entry_string)
            if self.camera:
                self.camera.set_img_path(self.img_path.get())
            self.update_fileviewer()

    def set_iso(self, event=None, silent=False):
        if self.camera:
            self.camera.set_iso(int(float(self.iso.get())))
        if not silent:
            self.report("iso set to " + self.iso.get())

    def set_shutter_speed(self, event=None, silent=False):
        if self.camera:
            self.camera.set_shutter_speed(int(float(self.shutter_speed.get()[2:])))
        if not silent:
            self.report("sh. speed set to " + self.shutter_speed.get())

    def set_file_format(self, event=None, silent=False):
        if self.camera:
            pass
            """set file format"""
        if not silent:
            self.report("file frmt set to " + "")

    def open_folder(self, event=None):
        if self.entry_img_path["state"] == NORMAL:
            selected_item = self.lbox_files.get(ACTIVE)
            selected_item += "/"
            if os.path.exists(self.img_path.get() + selected_item):
                if os.path.isdir(self.img_path.get() + selected_item):
                    # self.entry_img_path.config(text=self.img_path.get() + selected_item)
                    # self.set_img_path()
                    self.img_path.set(self.img_path.get() + selected_item)
                    self.update_fileviewer()

    def go_back_folder(self):
        if self.img_path.get().endswith("/"):
            target = "/".join(self.img_path.get().split("/")[:-2]) + "/"
        else:
            target = "/".join(self.img_path.get().split("/")[:-1]) + "/"
        if os.path.exists(target):
            if os.path.isdir(target):
                self.img_path.set(target)
                self.update_fileviewer()

    def choose_img_path(self):
        path = filedialog.askdirectory()
        if path:
            path += "/"
            self.img_path.set(path)
            self.update_fileviewer()

    def set_preset(self, event=None, silent=False):
        self.pmcu.set_preset(self.preset.get(), silent=silent)
        self.step_mode.set(get_step_mode_name(self.pmcu.step_mode))
        self.xstep.set(str(self.pmcu.step_size[0]))
        self.ystep.set(str(self.pmcu.step_size[1]))
        self.zstep.set(str(self.pmcu.step_size[2]))
        self.update_step_size_norm()

    def set_step_size(self, event=None, silent=True):
        self.pmcu.set_step_size([self.xstep.get(), self.ystep.get(), self.zstep.get()], silent=silent)
        self.update_step_size_norm()

    def update_step_size_norm(self, event=None, silent=True):
        xstep_norm, self.round_err[0] = norm_step(self.xstep.get(), self.pmcu.step_mode)
        ystep_norm, self.round_err[1] = norm_step(self.ystep.get(), self.pmcu.step_mode)
        zstep_norm, self.round_err[2] = norm_step(self.zstep.get(), self.pmcu.step_mode)
        self.xstep_norm.set(xstep_norm)
        self.ystep_norm.set(ystep_norm)
        self.zstep_norm.set(zstep_norm)
        self.update_round_error()

    def update_round_error(self):
        self.label_x_round_err.config(text="!" if self.round_err[0] else "")
        self.label_y_round_err.config(text="!" if self.round_err[1] else "")
        self.label_z_round_err.config(text="!" if self.round_err[2] else "")

    def set_step_size_norm(self, event=None, silent=True):
        self.xstep.set(int(self.xstep_norm.get() / get_norm_factor(self.pmcu.step_mode)))
        self.ystep.set(int(self.ystep_norm.get() / get_norm_factor(self.pmcu.step_mode)))
        self.zstep.set(int(self.zstep_norm.get() / get_norm_factor(self.pmcu.step_mode)))
        self.set_step_size()

    def set_step_mode(self, event=None, silent=False):
        self.pmcu.set_step_mode(self.step_mode.get(), silent=True)
        self.update_step_size_norm()
        if not silent:
            self.report("step mode set to " + self.step_mode.get())

    def report(self, message, end="\n", timestamp=True):
        self.text_log.config(state=NORMAL)
        if timestamp:
            timestamp = datetime.datetime.now().strftime("%H:%M:%S")
            message = timestamp + " " + message
        self.text_log.insert(END, message + end)
        self.text_log.config(state=DISABLED)
        self.text_log.see(END)

    def clear_log(self):
        self.text_log.config(state=NORMAL)
        self.text_log.delete("1.0", END)
        self.text_log.config(state=DISABLED)

