PRESETS = dict(
    precision=dict(step_mode="1/32", step_size=[1, 1, 1], iso=-1, shutter_speed=-1),
    x20=dict(step_mode="1/16", step_size=[50, 50, 200], iso=100, shutter_speed=640),
)
AVAILABLE = dict(
    iso=[-1, 100, 160, 400],
    shutter_speed=[-1, 640, 200, 160],
    step_mode=["1", "1/2", "1/4", "1/8", "1/16", "1/32"]
)

TX, TY, TZ = 30, 30, 30
TIME_NORM = True
DEFAULT_PRESET = "x20"

DEBUG_MODE = False

DIR_LEFT = 0
DIR_RIGHT = 1
DIR_UP = 2
DIR_DOWN = 3
DIR_IN = 4
DIR_OUT = 5
DIR_NAMES = ("left", "right", "up", "down", "in", "out")

