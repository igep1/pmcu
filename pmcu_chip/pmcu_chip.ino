//Test für z-Steuerung xx.11.19
// Schrittmotor-Steuerung
// DRV8825 Schrittmotortreiber
// Schritt: - Sleep (slp) auf HIGH verhindert Schlafmodus,  hat Pulldown
//          - nENBL(EN) auf LOW aktiviert H-Bruecke, hat Pulldown
//          - Rising edge bei STEP (stp) von LOW auf HIGH bewirkt Schritt,


// Seriell-Protokoll (alt)
// Ausgang:
// - Befehl "foto" loest Ausloesetrigger aus
// Eingang:
// - Befehl "z" gibt Bestaetigung, dass Bild uebertragen wurde
// - m100x passt Parameter fuer 100facher Vergroesserung an: Schrittzahl + Microstep-Einstellung
// -> m50x, m20x, m10x, m05x
// - "sXXYY", wobei XX Anzahl der Spalten und YY die Anzahl der Zeilen der Rasterung angibt
// - "nXXYY", wobei XX Anzahl der X-Schritte pro X-Rasterschritt und YY fuer Y-Rasterschritt
// microstepping-Modi:
// m0   0   1     0     1     0     1
// m1   0   0     1     1     0     0
// m2   0   0     0     0     1     1
//      1   1/2   1/4   1/8   1/16  1/32

#include <EEPROM.h>

// Adressen fuer EEPROM-Speicher
#define x_countstepADDR   0
#define y_countstepADDR   1
#define z_countstepADDR   2
#define x_dirADDR  3
#define y_dirADDR  4
#define z_dirADDR  5
#define x_numstepADDR 6
#define y_numstepADDR 7
#define z_numstepADDR 8
#define m0ADDR 9
#define m1ADDR 10
#define m2ADDR 11
#define pic_counter0ADDR 12
#define pic_counter1ADDR 13
#define x_numstep_toggle_compensationADDR 14
#define y_numstep_toggle_compensationADDR 15
#define z_numstep_toggle_compensationADDR 16

// Treiber-Steuerungspins
//#define xmot_flr  3
#define zmot_dir  2
#define zmot_stp  3
#define zmot_slp  7
#define xmot_dir  4
#define xmot_stp  5
#define xmot_slp  6
#define ymot_dir  8
#define ymot_stp  9
#define ymot_slp  10
#define mot_mod0  11
#define mot_mod1  12
#define mot_mod2  13
#define mot_flr  A0

bool m0 = 1;
bool m1 = 1;
bool m2 = 0;

//int t_step = 7000;
int tx_step = 30;
int ty_step = 30;
int tz_step = 30;
int wakeuptime = 100;

boolean x_dir = 0; 
boolean y_dir = 0;
boolean z_dir = 0;

String inString = "";
bool inStringComplete = false;

long int starttimer = millis();

void setup() {
  Serial.begin(9600);
  digitalWrite(xmot_slp, HIGH);
  digitalWrite(ymot_slp, HIGH);
  digitalWrite(zmot_slp, HIGH);
  setmot_mod();
  starttimer = millis();
  
}

void loop() {
  serialReceive();
  delay(100);
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    inString += inChar;
    if (inChar == '\n') {
      inStringComplete = true;
    }
  }
}

void serialReceive() {
  serialEvent();
  if (inStringComplete == true) {
    switch(int(inString[0])){
      case 'd':
        break;
      case 's':
        sleep();
        break;
      case 'x':
        move(int(inString[0]), xmot_stp, xmot_slp, tx_step);
        break;
      case 'y':
        move(int(inString[0]), ymot_stp, ymot_slp, ty_step);
        break;
      case 'z':
        move(int(inString[0]), zmot_stp, zmot_slp, tz_step);
        break;
      case 'm':
        set_mode();
    }
    Serial.println(inString);
    inString = "";
    inStringComplete = false;
  }
}

void move(int axis, int mot_stp, int mot_slp, int t_step) {
  int numstep = inString.substring(2, 6).toInt();
  //Serial.println(numstep);
  int dir = 1;
  if (inString[1] == '-') {
    dir = 0;
  }
  if (axis == 'x') {
    x_dir = dir;
  } else if (axis == 'y'){
    y_dir = dir;
  } else {
    z_dir = dir;
  }
  setmot_dir();
  int i = 0;
  
  //digitalWrite(mot_slp, HIGH);
  while(numstep - i > 0){
    togglepin(mot_stp);
    delay(t_step);
    togglepin(mot_stp);
    i++;
  }
  //digitalWrite(mot_slp, HIGH);
}


void set_mode(){
  int m = int(inString[1]);
  m0 = m & (1 << 0);
  m1 = m & (1 << 1);
  m2 = m & (1 << 2);
  setmot_mod();
}

void sleep() {
  togglepin(xmot_slp);
  togglepin(xmot_slp);
  if (digitalRead(xmot_slp) == 0){
      digitalWrite(xmot_slp, HIGH);
   } else{
      digitalWrite(xmot_slp, LOW);
   }
   
   if (digitalRead(xmot_slp) == 1){
      digitalWrite(ymot_slp, HIGH);
      digitalWrite(zmot_slp, HIGH);
   } else{
      digitalWrite(ymot_slp, LOW);
      digitalWrite(ymot_slp, LOW);
   }
}

void togglepin(int dirpin){
  if (digitalRead(dirpin) == 0){
    digitalWrite(dirpin, HIGH);
  } else{
    digitalWrite(dirpin, LOW);
  }
}

void setmot_dir(){
  digitalWrite(xmot_dir, x_dir);
  digitalWrite(ymot_dir, y_dir == 0);
  digitalWrite(zmot_dir, z_dir); 
}

void setmot_mod(){
    digitalWrite(mot_mod0, m0);
    digitalWrite(mot_mod1, m1);
    digitalWrite(mot_mod2, m2);
}
